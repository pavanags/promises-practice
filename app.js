const fs = require("fs");
// const { resolve } = require("path");

function xyz() {
    return new Promise((resolve, reject) => {
        fs.readFile("./data.json", (err, result) => {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

xyz().then((result) => {
    return writeFiles('./2.json', result)
}).then((res) => {
    fs.readFile("./3.json", "utf8", (err, result) => {
        if (err) {
            return writeFiles("./3.json", res);
        } else {
            console.log("File read was successful" + result);
        }
    });
}).then(() => {
    fs.readFile("./3.json", "utf8", (err) => {
        if (err) {
            console.error(err.message);
        } else {
            deleteFile("./3.json");
            console.log("File was successfully deleted")
        }
    });
}).catch((err) => {
    console.error(err.message);
});


function deleteFile(path) {
    return new Promise((resolve, reject) => {
        fs.unlink(path, (err) => {
            if (err) reject(err);
            resolve("Successfully deleted");
        })
    })
}


function writeFiles(path, res) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path, res, "utf8", (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(res);
            }
        })
    })
}